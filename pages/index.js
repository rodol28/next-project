import Head from 'next/head';
import Container from '../components/container';
import Users from '../components/users';
import isoFetch from 'isomorphic-fetch';

const Index = (props) => {
    return (
        <div>
            <Head>
                <title>Home</title>
            </Head>
            <Container>
                <h1>Users</h1>
                <Users users={props.users}/>
            </Container>
        </div>
    )
}

Index.getInitialProps = async (ctx) => {
    const res = await isoFetch('https://reqres.in/api/users');
    const resJSON = await res.json();
    return {users: resJSON.data};
}

export default Index;