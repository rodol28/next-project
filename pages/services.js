import Head from 'next/head';
import Container from '../components/container';

const Services = () => {
    return <div>
            <Head>
                <title>Services</title>
            </Head>
            <Container>
                <h1>Services</h1>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum deserunt et necessitatibus eum laborum ea! Aperiam asperiores libero consequuntur culpa, iure dicta porro eligendi quos, in doloremque fuga, quae cum.</p>
            </Container>
           </div>
}

export default Services;