import Head from 'next/head';
import Container from '../components/container';

const About = () => {
    return <div>
            <Head>
                <title>About</title>
            </Head>
            <Container>
                <h1>About</h1>
            </Container>
           </div>
}

export default About;