import { useRouter } from 'next/router';
import isoFetch from 'isomorphic-fetch';
import Container from '../../components/container';

const User = ({user}) => {
    const router = useRouter();
    const { id } = router.query;
    return (<Container>
                <div className="row">
                    <div className="col-md-6 offset-md-3">

                        <div className="card">
                            <div className="card-header text-center">
                                <img src={user.avatar} 
                                 className="card-img-top" 
                                 alt={user.last_name + user.first_name}
                                 style={{borderRadius: '50%', width: '8rem'}}
                                 />
                            </div>
                            <div className="card-body">
                                <h5 class="card-title text-center">{user.last_name}, {user.first_name}</h5>
                                <p className="card-text">Email: {user.email}</p>
                            </div>           
                        </div>

                    </div>
                </div>
            </Container>)
}

User.getInitialProps = async (ctx) => {
    const res = await isoFetch(`https://reqres.in/api/users/${ctx.query.id}`);
    const resJSON = await res.json();
    console.log(resJSON.data)
    return {user: resJSON.data};
}

export default User;