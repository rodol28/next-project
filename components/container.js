import Navigation from './navigation';
import Head from 'next/head';

const Container = (props) => {
    return <div>
            <Head>
                <link rel="stylesheet" href="https://bootswatch.com/4/materia/bootstrap.min.css"/>
            </Head>
            <Navigation/>
            <div className="container p-4">
                {props.children}
            </div>
            <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
           </div>
}

export default Container;