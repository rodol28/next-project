import Router from 'next/router';

const Users = (props) => {
    return (
        <ul>{props.users.map(user => {
            return (<li className="list-group-item list-group-item-action d-flex justify-content-between align-items-center" 
                        key={user.id}
                        onClick={e => Router.push('/users/[id]', `/users/${user.id}`)}>
                        <div>
                            <h5>{user.last_name}, {user.first_name}</h5>
                            <p>Email: {user.email}</p>
                        </div>
                        <img src={user.avatar} 
                             alt="avatar" 
                             style={{borderRadius: '50%'}}/>
                    </li>);
        })}</ul>
    )
}

export default Users;